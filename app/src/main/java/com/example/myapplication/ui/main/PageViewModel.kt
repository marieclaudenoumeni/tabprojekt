package com.example.myapplication.ui.main

import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel

class PageViewModel : ViewModel() {

    private val _index = MutableLiveData<Int>()
    val text: LiveData<Fragment> = Transformations.map(_index) {
        if ( it == 1) {
            BlankFragment1.newInstance("alksd", "alksdj")
        }

        if ( it == 2) {
            BlankFragment2.newInstance("alksd", "alksdj")
        }

        BlankFragment1.newInstance("alksd", "alksdj")    }

    fun setIndex(index: Int) {
        _index.value = index
    }
}